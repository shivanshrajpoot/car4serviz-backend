<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInServiceProviderRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_provider_request', function (Blueprint $table) {
            $table->integer('service_provider_id')->after('id');
            $table->integer('request_id')->after('service_provider_id');
            $table->string('request_type')->after('request_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_provider_request', function (Blueprint $table) {
            $table->dropColumn('service_provider_id');
            $table->dropColumn('request_id');
            $table->dropColumn('request_type');
        });
    }
}
