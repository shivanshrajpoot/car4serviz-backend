<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{

	use SoftDeletes;

	// protected $hidden = ['variant', 'model', 'brand'];

	protected $with = ['variant','type'];

	protected $appends = ['model', 'brand', 'display_name', 'model_name', 'brand_name', 'variant_name' ];

	/**
	 * Cutomer
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function customer()
	{
		return $this->belongsTo(Customer::class, 'customer_id');
	}

	/**
	 * Variant of vehicle
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function variant()
	{
		return $this->belongsTo(CarVariant::class, 'car_variant_id');
	}

	/**
	 * Variant of vehicle
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function brand()
	{
		return $this->variant->brand();
	}

	/**
	 * Variant of vehicle
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function model()
	{
		return $this->variant->model();
	}

	/**
	 * Model of Vehicle
	 * @return App\Models\Model
	 */
	public function getModelAttribute()
	{
		return $this->variant->model;
	}

	/**
	 * Brand Of Vehicle
	 * @return App\Models\Brand
	 */
	public function getBrandAttribute()
	{
		return $this->variant->model->brand;
	}

	public function getModelNameAttribute()
	{
		return $this->model->name;
	}

	public function getBrandNameAttribute()
	{
		return $this->brand->name;
	}

	public function getVariantNameAttribute()
	{
		return $this->variant->name;
	}	

	public function getDisplayNameAttribute()
	{
		return $this->brand->name." ".$this->model->name." ".$this->variant->name;
	}

	/**
	 * Car Type
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function type()
	{
		return $this->belongsTo(CarType::class, 'car_type_id');
	}

	/**
	 * Job Cards
	 * @return Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function jobCards()
	{
		return $this->hasMany(JobCard::class, 'vehicle_id');
	}
}