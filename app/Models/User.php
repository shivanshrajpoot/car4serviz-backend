<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email'
    ];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /** RELATIONS **/

    public function userable()
    {
        return $this->morphTo();
    }

    /** JWT REQUIRED **/
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getTypeAttribute()
    {
        if($this->userable instanceof ServiceProvider) return 'service_provider';
        if($this->userable instanceof Customer) return 'customer';
    }

    public function getFcmTokensArrayAttribute()
    {
        if(!$this->fcm_tokens) return [];

        return json_decode($this->fcm_tokens, true); 

    }
}
