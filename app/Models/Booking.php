<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
	 use SoftDeletes;

	 public function jobCard()
	 {
	 	return $this->belongsTo(JobCard::class);
	 }

	 public function bookingRequest()
	 {
	 	return $this->belongsTo(BookingRequest::class);
	 }

	 public function serviceProvider()
	 {
	 	return $this->belongsTo(ServiceProvider::class);
	 }

	 public function getStatusNameAttribute()
	 {
	 	return array_search($this->status,config('booking.status'));
	 }

}