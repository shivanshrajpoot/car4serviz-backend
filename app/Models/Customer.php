<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

	protected $fillable = ['first_name', 'last_name', 'profile_photo'];

    protected $appends = ['profile_photo_url'];

	/**
	 *  Get Eloquent Realtionship for User
	 */
	public function user()
    {
    	return $this->morphOne(User::class, 'userable');
    }

    public function getProfilePhotoUrlAttribute()
    {
        if($this->profile_photo) return env('APP_URL')."/profile_photos/".$this->profile_photo;
    }
    
    public function vehicles()
    {
        return $this->hasMany(Vehicle::class, 'customer_id');
    }

    /**
     * JobCards of Customer
     * @return HasManyThrough
     */
    public function jobCards()
    {
        return $this->hasManyThrough(JobCard::class, Vehicle::class);
    }
}