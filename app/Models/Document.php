<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{	
	/**
	 * Using soft deletes
	 */
	use SoftDeletes;

	protected $appends = ['url'];

    protected $visible = ['url'];

	public function documentable()
    {
        return $this->morphTo();
    }

    public function getUrlAttribute()
    {
    	return env('APP_URL').'/storage/'.$this->file_name;
    }

}