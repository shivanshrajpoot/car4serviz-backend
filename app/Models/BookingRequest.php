<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingRequest extends Model
{
	 use SoftDeletes;

	 protected $with = ['jobCard'];

	 public function jobCard()
	 {
	 	return $this->belongsTo(JobCard::class, 'job_card_id');
	 }

	 public function serviceProviders()
	 {
	 	return $this->morphToMany(ServiceProvider::class, 'request','service_provider_request');
	 }

	 public function booking()
	 {
	 	return $this->hasOne(Booking::class);
	 }

}