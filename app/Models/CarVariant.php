<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarVariant extends Model
{
	use SoftDeletes;

	protected $appends = ['fuel'];

    protected $hidden = ['fuel_type', 'model', 'brand'];

	public function model()
	{
		return $this->belongsTo(CarModel::class, 'car_model_id');
	}

	public function carType()
	{
		return $this->belongsTo(CarType::class, 'car_type_id');
	}

    public function getFuelAttribute(){
    	return array_search($this->fuel_type, config('car.fuel_type'));
    }

    public function brand()
    {
    	return $this->model->brand();
    }
 
}