<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarBrand extends Model
{
	use SoftDeletes;
	 public function models()
	 {
	 	return $this->hasMany(CarModel::class, 'car_brand_id');
	 }
}