<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CarType extends Model
{
	public function variants()
	{
		return $this->hasMany(CarVariant::class);
	}
	
}