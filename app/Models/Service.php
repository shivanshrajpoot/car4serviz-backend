<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	// protected $with = ['categories'];


    public function categories()
    {
    	return $this->hasMany(ServiceSubcategory::class);
    }
}
