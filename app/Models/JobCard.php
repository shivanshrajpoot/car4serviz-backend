<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobCard extends Model
{	
	/**
	 * Using soft deletes
	 */
	use SoftDeletes;

	protected $fillable = 
		[
			'location_lat',
			'location_long',
			'address',
			'service_datetime',
			'remark',
			'type',
			'status',
			'service_location'
		];

	protected $with = [
		'services',
		'serviceSubcategories',
		'vehicle',
		'documents'
	];

	/**
	 * Vehicle JobCard belongs to 
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function vehicle()
	{
	 	return $this->belongsTo(Vehicle::class, 'vehicle_id');
	}

	/**
	 * Documents
	 * @return Illuminate\Database\Eloquent\Relations\MorphMany
	 */
	public function documents()
	{
		return $this->morphMany(Document::class, 'documentable','owner_type', 'owner_id');
	}

	public function services()
	{
		return $this->belongsToMany(Service::class, 'job_card_service');
	}


	public function serviceSubcategories()
    {
    	return $this->belongsToMany(ServiceSubcategory::class, 'job_card_service_subcategory');
    }

    public function bookingRequest()
    {
    	return $this->hasOne(BookingRequest::class, 'job_card_id');
    }

    public function quoteRequest()
    {
    	return $this->hasOne(QuoteRequest::class, 'job_card_id');
    }

    public function booking()
    {
    	return $this->hasOne(Booking::class);
    }

    public function getServiceLocationNameAttribute()
    {
    	return array_search( $this->service_location, config('jobcard.location'));
    }

}