<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarModel extends Model
{
	use SoftDeletes;

	public function brand()
	{
		return $this->belongsTo(CarBrand::class, 'car_brand_id');
	}

	public function variants()
	{
		return $this->hasMany(CarVariant::class, 'car_model_id');
	}
}