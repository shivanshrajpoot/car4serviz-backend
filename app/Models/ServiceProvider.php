<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceProvider extends Model
{

    use SoftDeletes;

	protected $fillable = [
		'garage_name',
        'advisor_name',
		'phone',
		'lat',
		'long',
		'address_line_1',
		'address_line_2',
		'city',
		'state',
		'postal_code',
		'gst',
		'pickup_facility_available',
		'24_facility_available',
        'service_at_customer_location',
		'bank_name',
		'branch_name',
		'ifsc_code',
		'account_number',
        'step',
        'profile_workshop_photo',
        'service_at_customer_location'
	];

    /**
     * Attributes which are to be appened when toArray is called
     * @var Array
     */
	protected $appends = ['gst_available', 'workshop_photo_url'];

    /**
     * Attributes which are not to be sent in JSON Response
     * @var Array
     */
    protected $hidden = ['bank_name', 'branch_name', 'ifsc_code', 'account_number' ];


    public function carTypes()
    {
    	return $this->belongsToMany(CarType::class, 'service_provider_car_type')->withPivot('price');
    }

    public function serviceSubcategories()
    {
    	return $this->belongsToMany(ServiceSubcategory::class, 'service_provider_service_subcategory');
    }

    public function services()
    {
    	return $this->belongsToMany(Service::class, 'service_provider_service')->withPivot('discount');
    }

    public function user()
    {
    	return $this->morphOne(User::class, 'userable');
    }

    public function getGstAvailableAttribute()
    {
    	return ($this->gst) ? 1 : 0;
    }

    public function getWorkshopPhotoUrlAttribute()
    {
        if($this->profile_workshop_photo) return env('APP_URL')."/profile_workshop_photos/".$this->profile_workshop_photo;
    }

    public function getGeneralServiceCost($car_type_id)
    {
        return $this->carTypes()->where('car_types.id', $car_type_id)->first()->pivot->price;
    }

    public function getGeneralServiceDiscount()
    {
        return $this->services()->find(1)->pivot->discount;
    }

    public function bookingRequests()
    {
        return $this->morphedByMany(BookingRequest::class, 'request','service_provider_request')->where('booking_requests.status', config('request.status.active'));
    }

    public function quoteRequests()
    {
        return $this->morphedByMany(QuoteRequest::class, 'request','service_provider_request')->where('quote_requests.status', config('request.status.active'));
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function quotes()
    {
        return $this->hasMany(Quote::class);
    }
}