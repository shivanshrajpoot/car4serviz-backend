<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quote extends Model
{
	 use SoftDeletes;

	 public function quoteRequest()
	 {
	 	return $this->belongsTo(QuoteRequest::class);
	 }

	 public function serviceProvider()
	 {
	 	return $this->belongsTo(ServiceProvider::class);
	 }

}