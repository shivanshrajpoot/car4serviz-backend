<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuoteRequest extends Model
{
	 use SoftDeletes;

	 public function serviceProviders()
	 {
	 	return $this->morphToMany(ServiceProvider::class, 'request','service_provider_request');
	 }

	 public function jobCard()
	 {
	 	return $this->belongsTo(JobCard::class);
	 }

	 public function quotes()
	 {
	 	return $this->hasMany(Quote::class);
	 }

}