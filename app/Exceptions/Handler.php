<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->is('api/*')) {
            switch ($exception) {
                case ($exception instanceof ValidationException):
                    return response()->error([
                        'validation_errors' => $exception->errors(),
                        'message' => 'Input data is not valid'
                    ], 422);
                break;

                case ($exception instanceof AuthenticationException):
                    return response()->error([
                        'message' => 'You must be logged in'
                    ], 401);
                break;

                case ($exception instanceof MethodNotAllowedHttpException):
                    return response()->error([
                        'message' => 'Method Not Allowed'
                    ], 405);
                break;

                case ($exception instanceof InvalidOtpException): 
                    return response()->error([
                         'message' => 'Invalid Otp'
                    ], 422);

                case ($exception instanceof LocationNotServiceableException):
                    return response()->error([
                        'message' => 'No Garages Nearby'
                    ],404);

                case ($exception instanceof AccessException): 
                    return response()->error([
                         'message' => 'You are not authorized to access these resources'
                    ], 403);

                case ($exception instanceof AuthorizationException): 
                    return response()->error([
                         'message' => 'You are not authorized to access these resources'
                    ], 403);                
                
            }
        }

        return parent::render($request, $exception);
    }
}
