<?php

namespace App\Exceptions;

class LocationNotServiceableException extends \Exception {}