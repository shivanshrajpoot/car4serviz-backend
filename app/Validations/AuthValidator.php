<?php

namespace App\Validations;

use App\Traits\ValidatorTrait;
use App\Contracts\ValidatorContract;

class AuthValidator implements ValidatorContract 
{
	use ValidatorTrait;

	public function rules($inputs, $type = '', $data = [])
	{
		switch ($type) {
			case 'register' : 

				return [
					'name' => 'string|max:255',
					'email' => 'string|email|max:255|unique:users',
					'password' => 'required|min:6|max:255',
					'phone' => 'required|numeric|digits:12|unique:users',
					'user_type' => 'required|numeric|in:0,1'
				];

			break;

			case 'login' : 
				return [
					'phone' => 'required|numeric|digits:10|unique:users',
					'password' => 'required|min:6|max:255',
				];
			break;

			case 'otp' : 
				return [
					'phone' => 'required|numeric|digits_between:9,12'
				];
			break;
		}
	}
}