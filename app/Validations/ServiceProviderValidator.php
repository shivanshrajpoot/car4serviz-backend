<?php

namespace App\Validations;

use App\Traits\ValidatorTrait;
use App\Contracts\ValidatorContract;

class ServiceProviderValidator implements ValidatorContract 
{
	use ValidatorTrait;

	public function messages()
	{
		return [
			'lat.required' => 'Please select location on map',
			'long.required' => 'Please select location on map',
			'car_types.*.price.required' => 'Price is required for every car type.',
			'car_types.*.price.numeric' => 'Price should be numeric',
			'car_types.*.price.min' => 'Price should be atleast 10 Rs.',
			'car_types.*.price.max' => 'Price should be atmost 1000000 Rs.',
			'services.*.discount.required' => 'Discount is required for every service you provide.',
			'services.*.discount.numeric' => 'Discount should be numeric and must be between 0 to 100',
			'services.*.discount.min' => 'Discount should be numeric and must be between 0 to 100',
			'services.*.discount.max' => 'Discount should be numeric and must be between 0 to 100',
		];
	}

	public function rules($inputs, $type = '', $data = [])
	{
		switch ($type) {
			case 'edit' :

				return [
						'garage_name' => 'string|max:255|min:2|nullable',
						'advisor_name' => 'string|max:255|min:2|nullable',
						'phone' => 'numeric|digits:10|nullable',
						'lat' => 'numeric|nullable',
						'long' => 'numeric|nullable',
						'address_line_1' => 'string|max:255|min:6|nullable',
						'address_line_2' => 'string|max:255|min:6|nullable',
						'city' => 'string|max:255|min:6|nullable',
						'state' => 'string|max:255|min:6|nullable',
						'postal_code' => 'numeric|nullable',
						'gst' => 'string|max:255|min:6|nullable',
						'pickup_facility_available' => 'boolean|nullable',
						'24_facility_available' => 'boolean|nullable',
						'bank_name' => 'string|max:255|min:6|nullable',
						'branch_name' => 'string|max:255|min:6|nullable',
						'ifsc_code' => 'string|max:255|min:6|nullable',
						'account_number' => 'string|max:255|min:6|nullable',
						'service_categories' => 'array|nullable',
						'service_categories.*' => 'exists:service_categories,id',
						// 'service_categories.*.discount' => 'numeric|max:100|min:0|required',
						'services' => 'array|nullable|min:1',
						'services.*.id' => 'exists:services,id',
						'services.*.discount' => 'numeric|max:100|min:0|nullable',
						'car_types' => 'array|nullable|min:1',
						'car_types.*.id' => 'exists:car_types,id',
						'car_types.*.price' => 'numeric|max:1000000|min:10|nullable',
					];
				break;
			case 'edit.1' : 
				return [
					'lat' => 'numeric|required',
					'long' => 'numeric|required',
					'address_line_1' => 'required|string|max:255|min:6',
					'address_line_2' => 'required|string|max:255|min:6',
					'city' => 'required|string|max:255|min:3',
					'state' => 'required|string|max:255|min:3',
					'postal_code' => 'required|numeric',
				];
			case 'edit.2' :
				return [
					'garage_name' => 'string|max:255|min:2|required',
					'advisor_name' => 'string|max:255|min:2|required',
					'email' => 'string|email|max:255',
					'gst' => 'required_if:gst_available,1|nullable|max:255|min:6',
				];
			break;

			case 'edit.3' : 
				return [
					'profile_workshop_photo' => 'required',
				];
			break;

			case 'edit.4' : 
				return [
					'services' => 'array|required|min:1',
					'services.*.id' => 'exists:services,id',
					'services.*.discount' => 'numeric|max:100|min:0|required',
					'service_subcategories' => 'array|required|min:1',
					'service_subcategories.*' => 'exists:service_subcategories,id',
				];
			break;

			// use general edit API for step 3

			case 'edit.5' : 
				return [
					'pickup_facility_available' => 'boolean|required',
					'24_facility_available' => 'boolean|required',
					// 'profile_workshop_photo' => 'boolean|required',
					'service_at_customer_location' => 'boolean|required',
					'car_types' => 'array|required|min:1',
					'car_types.*.id' => 'exists:car_types,id',
					'car_types.*.price' => 'numeric|max:1000000|min:10|required'
				];
			break;

			case 'edit.6' : 

				return [
					'bank_name' => 'string|max:255|min:6|required',
					'branch_name' => 'string|max:255|min:6|required',
					'ifsc_code' => 'string|max:255|min:6|required',
					'account_number' => 'string|max:255|min:6|required',
				];
			break;
		}
	}

	public function rulesBkp($inputs, $type = '', $data = [])
	{
		switch($type) {
			case 'edit.1' : 
				return [
					'profile_photo' => 'required|string',
					'name' => 'string|max:255|min:6|required'
				];
			case 'edit.2' : 
				return [
					'workshop_photo' => 'required|string',
				];
			case 'edit.3' : 
				return [
					'phone' => 'required|numeric|digits:10',
					'gst' => 'required_if:gst_available,1|nullable|max:255|min:6',
					'24_facility_available' => 'boolean|required',
					'pickup_facility_available' => 'boolean|required',
				];
			case 'edit.8' : 
				return [
					'lat' => 'numeric|required',
					'long' => 'numeric|required',
					'address_line_1' => 'required|string|max:255|min:6',
					'address_line_2' => 'required|string|max:255|min:6',
					'city' => 'required|string|max:255|min:3',
					'state' => 'required|string|max:255|min:3',
					'postal_code' => 'required|numeric',
				];
			case 'edit.4' : 
				return [
					'car_types' => 'array|required',
					'car_types.*.id' => 'exists:car_types,id',
					'car_types.*.price' => 'numeric|max:1000000|min:10|required'
				];
			case 'edit.6' : 
				return [
					'services' => 'array|required|min:1',
					'services.*.id' => 'exists:services,id',
					'services.*.discount' => 'numeric|max:100|min:0|required',
				];
			case 'edit.7' : 
				return [
					'service_categories' => 'array|required|min:1',
					'service_categories.*' => 'exists:service_categories,id',
				];
			case 'edit.5' : 

				return [
					'bank_name' => 'string|max:255|min:6|required',
					'branch_name' => 'string|max:255|min:6|required',
					'ifsc_code' => 'string|max:255|min:6|required',
					'account_number' => 'string|max:255|min:6|required',
				];
			break;

			case 'edit' :

				return [
						'name' => 'string|max:255|min:6|nullable',
						'phone' => 'numeric|digits:10|nullable',
						'lat' => 'numeric|nullable',
						'long' => 'numeric|nullable',
						'address_line_1' => 'string|max:255|min:6|nullable',
						'address_line_2' => 'string|max:255|min:6|nullable',
						'city' => 'string|max:255|min:6|nullable',
						'state' => 'string|max:255|min:6|nullable',
						'postal_code' => 'numeric|nullable',
						'gst' => 'string|max:255|min:6|nullable',
						'pickup_facility_available' => 'boolean|nullable',
						'24_facility_available' => 'boolean|nullable',
						'bank_name' => 'string|max:255|min:6|nullable',
						'branch_name' => 'string|max:255|min:6|nullable',
						'ifsc_code' => 'string|max:255|min:6|nullable',
						'account_number' => 'string|max:255|min:6|nullable',
						'service_categories' => 'array|nullable',
						'service_categories.*' => 'exists:service_categories,id',
						// 'service_categories.*.discount' => 'numeric|max:100|min:0|required',
						'services' => 'array|nullable',
						'services.*.id' => 'exists:services,id',
						'services.*.discount' => 'numeric|max:100|min:0|nullable',
						'car_types' => 'array|nullable',
						'car_types.*.id' => 'exists:car_types,id',
						'car_types.*.price' => 'numeric|max:1000000|min:10|nullable',
					];

		}
	}
}