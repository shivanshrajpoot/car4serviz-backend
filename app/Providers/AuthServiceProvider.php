<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('job_card.create', function($user, $vehicle) {
            return $user->userable->id == $vehicle->customer->id;
        });

        Gate::define('booking_request.accept', function($user, $booking_request) {
            return $user->userable->bookingRequests->contains($booking_request->id);
        });
        Gate::define('quote.create', function($user, $quote_request) {
            return $user->userable->quoteRequests->contains($quote_request->id);
        });
        Gate::define('quotes.index', function($user, $job_card) {
            return $user->userable->id === $job_card->vehicle->customer->id;
        });
    }
}
