<?php

namespace App\Repositories;

use App\Models\JobCard;

class JobCardRepository {

	protected $model = JobCard::class; 

	public function getAll() {
		return $this->model::get();
	}
}