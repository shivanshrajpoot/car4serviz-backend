<?php

namespace App\Repositories;

use App\Models\ServiceProvider;
use Illuminate\Pagination\LengthAwarePaginator;

class ServiceProviderRepository {

	protected $model = ServiceProvider::class;

	public function getAll()
	{
		return $this->model::with('carTypes')->get();
	}

	public function getAllPaginated($perPage = 15)
	{
		return $this->model::with('carTypes')->with('serviceSubcategories')->paginate($perPage);
	}

	public function getPaginatedNearbyServiceProviders($latitude, $longitude, $distance_range, $perPage)
	{
		
		$page = array_get(request()->all(), 'page', 1);

		$service_providers = $this->model::query()
		                   ->selectRaw("*, (ST_Distance_Sphere( point(`long`, `lat`), point(? , ?)) * .001) AS distance", [$longitude, $latitude])
		                   ->having('distance','<=', $distance_range)
		                   ->orderBy('distance')
		                   ->with('carTypes')
		                   ->with('serviceSubcategories')
		                   ->get()->toArray();
		 
		 $total_service_providers = count($service_providers);

		 $service_providers = array_slice($service_providers, ($page -1)*$perPage, $perPage);

		 return new LengthAwarePaginator($service_providers, $total_service_providers, $perPage, $page, ['path' => request()->fullUrl()]);

		 // Faster method
		 // $normalized_range = $distance_range/85;
		 // return $this->model::whereBetween('lat', [ $latitude - $normalized_range, $latitude + $normalized_range ])
					// ->whereBetween('long', [ $longitude - $normalized_range, $longitude + $normalized_range ])
					// ->paginate($perPage);
	}

	public function getNearbyServiceProviders($latitude, $longitude, $distance_range)
	{
		return  $this->model::query()
		                   ->selectRaw("*, (ST_Distance_Sphere( point(`long`, `lat`), point(? , ?)) * .001) AS distance", [$longitude, $latitude])
		                   ->having('distance','<=', $distance_range)
		                   ->orderBy('distance')
		                   ->get();

	}
}