<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository {

	public function getByPhone($phone) {
		return User::where('phone', $phone)->first();
	}
}