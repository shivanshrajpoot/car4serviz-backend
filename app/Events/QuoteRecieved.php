<?php

namespace App\Events;

use App\Models\Quote;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class QuoteRecieved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Varibale Stores Quote
     * @var Quote
     */
    public $quote;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Quote $quote)
    {
        \Log::info("----Event:QuoteRecieved----");
        \Log::info($quote);
        \Log::info("----Event:QuoteRecieved----");
        $this->quote = $quote;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
