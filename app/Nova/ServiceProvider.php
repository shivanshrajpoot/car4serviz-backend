<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

/**
 * Fields
 */
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\MorphOne;

class ServiceProvider extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\ServiceProvider';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'garage_name';

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Users';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Garage Name')
                    ->rules('required'),

            Text::make('Advisor Name')
                    ->rules('required'),

            Number::make('Latitude', 'lat')
                    ->min(1)
                    ->step(0.00000001)
                    ->rules('required')
                    ->hideFromIndex(),

            Number::make('Longitude', 'long')
                    ->min(1)
                    ->step(0.000000001)
                    ->rules('required')
                    ->hideFromIndex(),

            Text::make('Location', function () {
                if(!($this->lat && $this->long) ) return "";
                return '<span><a target="_blank" href="https://www.google.com/maps/search/?api=1&query='.$this->lat.','.$this->long.'"
                class="no-underline dim text-primary font-bold">Location</a></span>';
            })->asHtml(),            

            MorphOne::make('User')

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
