<?php

namespace App\Services;

use Image;

use App\Models\JobCard;
use App\Models\Vehicle;
use App\Models\Document;

use App\Repositories\JobCardRepository;

use App\Exceptions\LocationNotServiceableException;

class JobCardService
{
	/**
	 * Constructer
	 * @param JobCardRepository $jobCardRepository
	 */
	public function __construct(
		JobCardRepository $jobCardRepository,
		ServiceProviderService $serviceProviderService,
		BookingRequestService $bookingRequestService,
		QuoteRequestService $quoteRequestService,
		NotificationService $notificationService)
	{
		$this->jobCardRepository = $jobCardRepository;
		$this->serviceProviderService = $serviceProviderService;
		$this->bookingRequestService = $bookingRequestService;
		$this->quoteRequestService = $quoteRequestService;
		$this->notificationService = $notificationService;
	}

	public function create($customer, $vehicle, $inputs)
	{
		/**
		 * Check if there are any service providers nearby
		 */
		if(!array_get($inputs, 'service_provider_id')) {
			$nearby_service_providers = $this->serviceProviderService->getNearbyServiceProviders( array_get($inputs, 'location_lat'), array_get($inputs, 'location_long'), env('DEFAULT_DISTANCE_FILTER_RANGE', 8));

			if( !$nearby_service_providers || count($nearby_service_providers ) == 0) throw new LocationNotServiceableException();
		}
			
		$inputs['type'] = $this->getJobCardType(array_get($inputs, 'services'));

		$jobCard = $vehicle->jobCards()->create($inputs);

		$images = array_get($inputs,'images');

		/**
		 * Save Media Files for the JobCard
		 */

		if($images){

			foreach($images as $image){

				$fileName = md5(microtime(true)* 10000). mt_rand(10000, 99999);
				
				/**
				 * Save Original Uploaded Image
				 */
				$originalFilePath = storage_path('app/' . $fileName);
				Image::make($image)->save($originalFilePath);

				/**
				 * Save Optimized version of Image
				 */
				$optimizedFilePath = storage_path('app/public/' . $fileName);
				Image::make($image)->resize(500,null,function ($constraint) {
    				$constraint->aspectRatio();
    			})->save($optimizedFilePath);

				/**
				 * Create Database Entry of Image
				 * @var Document
				 */
				$document = new Document;
				$document->file_name = $fileName;

				$document->file_path = 'storage/app/public'.$fileName;

				$jobCard->documents()->save($document);

			}

			
		}


		$jobCard->services()->sync( array_get($inputs, 'services', []) );

		$jobCard->serviceSubcategories()->sync( array_get($inputs, 'service_subcategories', []) );

		return $this->createRequest($jobCard, $vehicle, array_get($inputs, 'service_provider_id'));
	}

	/**
	 * Determine the type of JobCard
	 * @param  Array $services
	 * @return int
	 */
	protected function getJobCardType($services)
	{
		if(in_array(5, $services))
		{
			return config('jobcard.type.roadside');
		}
		if(in_array(1, $services) && count($services) == 1)
		{
			return config('jobcard.type.general');
		}

		return config('jobcard.type.other');
	}

	/**
	 * Intial JobCard Status for Different JobCard Types
	 * @param  string $type 
	 * @return number
	 */
	protected function getInitialStatus($type)
	{
		return config('jobcard.inital_status')[$type];
	}

	/**
	 * Create a Request for JobCard
	 * @param  JobCard $jobCard             
	 * @param  Vehicle $vehicle             
	 * @param  number  $service_provider_id 
	 * @return JobCard                       
	 */
	public function createRequest(JobCard $jobCard, Vehicle $vehicle, $service_provider_id = null)
	{

		
		if($jobCard->type == config('jobcard.type.general')) {

			$serviceProvider = $this->serviceProviderService->get($service_provider_id);

			$booking_request = $this->bookingRequestService->create($jobCard, $serviceProvider);

			$jobCard->status = $this->getInitialStatus($jobCard->type);

			$jobCard->save();

			$jobCard->booking_request;

			// $booking_request->jobCard;

			$payload = $this->formatBookingRequestForNotification($booking_request);

			$this->notificationService->sendDataMessage($serviceProvider->user, $payload);

			return $jobCard;
		}

		if( $jobCard->type == config('jobcard.type.other') )
		{
			$quote_request = $this->quoteRequestService->create($jobCard);

			$jobCard->status = $this->getInitialStatus($jobCard->type);

			$jobCard->save();

			$jobCard->quoteRequest;

			return $jobCard;
		}

	}

	protected function formatBookingRequestForNotification($booking_request)
	{

		$job_card = $booking_request->jobCard;

		$formatted_services = [];

		foreach($job_card->services as $service) {

			$subcategories = $job_card->serviceSubcategories
									 ->where('service_id', $service->id)
									 ->pluck('name')
									 ->toArray();
			
			$formatted_services[$service->name] = $subcategories;
		}

		$payload = [
			'type' => 'BOOKING_REQUEST',
			'id' => $booking_request->id,
			'status' => array_search( $booking_request->status ,config('request.status') ),
			'vehicle_name' => $job_card->vehicle->display_name,
			'latitude' => $job_card->location_lat,
			'longitude' => $job_card->location_long,
			'service_datetime' => $job_card->service_datetime,
			'remark' => $job_card->remark,
			'services' => $formatted_services,
			'service_location' => ucfirst (array_search( $job_card->service_location , config('jobcard.location') ))
		];

		return $payload;
	}


}