<?php

namespace App\Services;

use FCM;
use Log;
use LaravelMsg91;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;


use App\Models\User; 

class NotificationService
{
	public function __construct()
	{

	}

	/**
	 * Save FCM token of device
	 * @param  User   $user      
	 * @param  string $fcm_token 
	 * @return User $user;
	 */
	public function saveFcmToken(User $user, $new_fcm_token)
	{
		$fcm_tokens_json = $user->fcm_tokens;

		if(!$fcm_tokens_json) $fcm_tokens = [];
		else {
			$fcm_tokens = json_decode($fcm_tokens_json, true);
		}

		if(array_search($new_fcm_token, $fcm_tokens) !== false) {
			return $user;
		}

		array_push($fcm_tokens, $new_fcm_token);
		
		$user->fcm_tokens = json_encode($fcm_tokens);

		$user->save();

		return $user;

	}

	public function removeFcmToken(User $user, $fcm_token)
	{
		$fcm_tokens_json = $user->fcm_tokens;

		if(!$fcm_tokens_json) return $user;

		$fcm_tokens = json_decode($fcm_tokens_json, true);
		
		$token_index = array_search($fcm_token, $fcm_tokens);

		if($token_index === false) return $user;

		array_splice($fcm_tokens, $token_index, 1);
		$user->fcm_tokens = json_encode($fcm_tokens);
		$user->save();

		return $user;

	}

	/**
	 * Notify a User's via FCM
	 * @param  User   $user  
	 * @param  string $title 
	 * @param  string $body  
	 * @param  array  $data  
	 * @return 
	 */
	public function notify(User $user, $title = null, $body = null,array $data = null)
	{
		$fcm_tokens_json = $user->fcm_tokens;

		if( !$fcm_tokens_json ) return false;

		$fcm_tokens = json_decode($user->fcm_tokens, true);

		if(count($fcm_tokens) == 0) return false;


		$optionBuilder = new OptionsBuilder();
		$optionBuilder->setPriority('high');
		$option = $optionBuilder->build();

		$notificationBuilder = new PayloadNotificationBuilder();
		$notificationBuilder->setTitle($title);
		$notificationBuilder->setBody($body);
		$notification = $notificationBuilder->build();

		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData($data);
		$data = $dataBuilder->build();

		Log::info('=========Sending Notification=========');
		Log::info('----Notification----');
		Log::info($notification);
		Log::info('---- Notification ----');
		Log::info('----Data----');
		Log::info($data);
		Log::info(json_encode($data));
		Log::info('---- Data ----');
		Log::info('----FCM Tokens----');
		Log::info($fcm_tokens);
		Log::info('---- FCM Tokens ----');

		$downstreamResponse = FCM::sendTo($fcm_tokens, $option, $notification, $data);
		
		Log::info('----FCM Response----');
		// Log::info($downstreamResponse);
		Log::info('---- FCM Response ----');		
		Log::info('========= Notification Sent =========');


		return $downstreamResponse;		

	}

	public function sendDataMessage(User $user, $data)
	{
		$fcm_tokens_json = $user->fcm_tokens;

		if(!$fcm_tokens_json) return false;

		$fcm_tokens = json_decode($user->fcm_tokens, true);

		if(count($fcm_tokens) == 0) return false;


		$optionBuilder = new OptionsBuilder();
		$optionBuilder->setPriority('high');
		$option = $optionBuilder->build();

		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData($data);
		$data = $dataBuilder->build();

		Log::info('=========Sending Data Notification=========');
		Log::info('----Data----');
		Log::info($data);
		Log::info(json_encode($data));
		Log::info('---- Data ----');
		Log::info('----FCM Tokens----');
		Log::info($fcm_tokens);
		Log::info('---- FCM Tokens ----');

		$downstreamResponse = FCM::sendTo($fcm_tokens, $option, null, $data);

		Log::info('----FCM Response----');
		// Log::info($downstreamResponse);
		Log::info('---- FCM Response ----');		
		Log::info('========= Data Notification Sent =========');

		return $downstreamResponse;



	}

}