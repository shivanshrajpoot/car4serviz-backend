<?php

namespace App\Services;

class PhoneService {


	public function getWithCountryCode($phone)
    {
        if(substr($phone, 0, 2) != '91') {
            $phone = '91' . $phone;
        }
        return $phone;
    }

}