<?php

namespace App\Services;

use App\Models\BookingRequest;

class HelperSevice
{
	public function mapServicesAndCategories($job_card)
	{

		$formatted_services = [];

		foreach($job_card->services as $service) {

			$subcategories = $job_card->serviceSubcategories
									 ->where('service_id', $service->id)
									 ->pluck('name')
									 ->toArray();
			
			$formatted_services[$service->name] = $subcategories;
		}

		return $formatted_services;

	}

}