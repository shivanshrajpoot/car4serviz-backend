<?php

namespace App\Services;

use Image;
use App\Models\Service;
use App\Models\CarType;
use Illuminate\Http\Request;
use App\Models\ServiceProvider;
use App\Repositories\ServiceProviderRepository;

class ServiceProviderService
{
	function __construct(ServiceProviderRepository $serviceProviderRepository)
	{
		$this->serviceProviderRepository = $serviceProviderRepository;
	}


	/**
	 * Get All Service Providers
	 * @return Collection
	 */
	public function getAll()
	{
		return $this->serviceProviderRepository->getAll();
	}

	/**
	 * Get Paginated list of Service Providers within a distance
	 * @param  Decimal  $latitude  			Requestor's Latitude
	 * @param  Decimal  $longitude 			Requestor's Longitude
	 * @param  integer  $distance_range  	Distance Range
	 * @return Collection of Service Providers
	 */
	public function getNearbyServiceProviders($latitude, $longitude, $distance_range)
	{
		return $this->serviceProviderRepository->getNearbyServiceProviders($latitude, $longitude, $distance_range);
	}

	/**
	 * Get All Paginated Service Provider
	 * @param  integer $perPage Number of Records Per Page
	 * @return LengthAwarePaginator
	 */
	public function getAllPaginated($perPage = 15)
	{
		return $this->serviceProviderRepository->getAllPaginated($perPage);
	}

	/**
	 * Get Paginated list of Service Providers within a distance
	 * @param  Decimal  $latitude  			Requestor's Latitude
	 * @param  Decimal  $longitude 			Requestor's Longitude
	 * @param  integer  $distance_range  	Distance Range  
	 * @param  integer  $perPage   			Number of Records Per Page
	 * @return LengthAwarePaginator
	 */
	public function getPaginatedNearbyServiceProviders($latitude, $longitude, $distance_range, $perPage = 15)
	{
		return $this->serviceProviderRepository->getPaginatedNearbyServiceProviders($latitude, $longitude, $distance_range, $perPage);
	}

	public function get($id)
	{
		return ServiceProvider::findOrFail($id);
	}

	public function edit(ServiceProvider $sp, $inputs, $step=null)
	{

		if($step) {
			$validation = 'edit' . '.' . $step;
			$inputs['step'] = $step;
		}

		if(isset($inputs['profile_workshop_photo'])) {
	        $fileName = md5(microtime(true)* 10000). mt_rand(10000, 99999);
	        $filePath = public_path() . "/profile_workshop_photos/" . $fileName;
			Image::make($inputs['profile_workshop_photo'])->save($filePath);  
	        $inputs['profile_workshop_photo'] = $fileName;
		}

		$sp->update($inputs);

		if(isset($inputs['services'])) {
			$i = [];
			foreach ($inputs['services'] as $c) {
				$i[$c['id']] = [ 'discount' => $c['discount'] ];
			}
			$sp->services()->sync($i);
		}

		if(isset($inputs['service_subcategories'])) {

			$service_subcategories = [];
			
			foreach ($inputs['service_subcategories'] as $subcategory) {
				$service_subcategories[$subcategory['id']] = [];
			}
			
			$sp->serviceSubcategories()->sync($service_subcategories);
		}

		if(isset($inputs['car_types'])) {
			$i = [];
			foreach ($inputs['car_types'] as $c) {
				$i[$c['id']] = [ 'price' => $c['price'] ];
			}

			$sp->carTypes()->sync($i);
		}

		return $sp;
	}
}
