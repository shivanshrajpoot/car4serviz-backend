<?php

namespace App\Services;

use Image;
use App\Models\Customer;


class CustomerService
{
	
	public function __construct()
	{

	}

	/**
	 * Update Customer
	 * @return App
	 */
	public function update($customer, $inputs)
	{
		if(isset($inputs['profile_photo'])) {
			$fileName = md5(microtime(true)* 10000). mt_rand(10000, 99999);
	        $filePath = public_path() . "/profile_photos/" . $fileName;
			Image::make($inputs['profile_photo'])->save($filePath);  
	        $inputs['profile_photo'] = $fileName;
		}

		$customer->update($inputs);

		if(isset($inputs['email'])) {
			$customer->user->update($inputs);			
		}
		
		return $customer;
	}



	public function make(){
		
		$path = storage_path('app/public/'.'147');
		\Log::info($path);
		Image::make($image)
		     ->resize(500,null,function ($constraint) {
    				$constraint->aspectRatio();
    			})
		     ->save($path);

		return $path;
	}
}