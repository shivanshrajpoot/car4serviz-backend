<?php

namespace App\Services;

/**
 * Model Imports
 */
use App\Models\Quote;
use App\Models\QuoteRequest;
use App\Models\ServiceProvider;

/**
 * Event Imports
 */
use App\Events\QuoteRecieved;

class QuoteService
{

	public function __construct()
	{

	}

	/**
	 * Create A Quote
	 * @param  QuoteRequest 	 $quote_request 
	 * @param  Array        	 $inputs
	 * @param  ServiceProvider   $service_provider        
	 * @return Quote
	 */
	public function create(QuoteRequest $quote_request, ServiceProvider $service_provider, Array $inputs)
	{
		$quote = new Quote;
		$quote->total_amount = array_get($inputs, 'total_amount');
		$quote->quote_request_id = $quote_request->id;
		$service_provider->quotes()->save($quote);

		event(new QuoteRecieved($quote) );

		return $quote;

	}
	
}