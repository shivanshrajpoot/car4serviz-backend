<?php

namespace App\Services;

use App\Models\BookingRequest;

class BookingRequestService
{
	public function create($jobCard, $serviceProvider, $total_amount = null)
	{
		$bookingRequest = new BookingRequest;

		if($total_amount) {
			$bookingRequest->total_amount = $total_amount;			
		}
		else {
			$bookingRequest->total_amount = $this->getTotalAmount($serviceProvider, $jobCard);
		}

		$bookingRequest->status = config('request.status.active');

		$jobCard->bookingRequest()->save($bookingRequest);

		$serviceProvider->bookingRequests()->attach($bookingRequest->id);

		return $bookingRequest;


	}


	protected function getTotalAmount($serviceProvider, $jobCard)
	{
		$price = $serviceProvider->getGeneralServiceCost($jobCard->vehicle->car_type_id);
		$discount_percent = $serviceProvider->getGeneralServiceDiscount();

		return $price - ($price * $discount_percent)/100;
	}
}