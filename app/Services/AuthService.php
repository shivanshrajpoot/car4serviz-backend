<?php

namespace App\Services;

use Hash;
use LaravelMsg91;

use App\Models\User;
use App\Models\Customer;
use App\Models\ServiceProvider;

use App\Services\PhoneService;

use App\Validations\AuthValidator;

use App\Exceptions\InvalidOtpException;

use App\Repositories\UserRepository;

class AuthService
{

	/**
	 * Initialize with dependencies
	 * @param AuthValidator $authValidator Validates input data
	 * @param LaravelMsg91  $msg91         Message service
	 * @param PhoneService  $phoneService  Phone number formats
	 */
	public function __construct(AuthValidator $authValidator, LaravelMsg91 $msg91, PhoneService $phoneService)
	{

		$this->authValidator = $authValidator;

        $this->msg91 = $msg91;

        $this->phoneService = $phoneService;
	}


	/**
	 * 
	 * @param  $phone_number
	 * @return boolean
	 */
	public function sendOtp($phone_number)
	{
		$otp = mt_rand(1000,9999);
		
		$message = "Your One Time Verification code for Car4Serviz is ".$otp.". Please do not share it with anyone.";

        $this->msg91->sendOtp($phone_number, $otp, $message);
        return true;
	}

	/**
	 * 
	 * @param   $phone_number
	 * @return 	boolean
	 */
	public function resendOtp($phone_number)
	{

		$this->msg91->resendOtp($phone_number);

		return true;
	}

	/**
	 * Register a new User
	 * @param  Array  $inputs [description]
	 * @return [type]         [description]
	 */
	public function register(Array $inputs)
	{
		$this->verifyOtp($inputs['phone'], $inputs['otp']);
		
		if($inputs['user_type'] == config('user.type.service_provider')){
			$user = $this->registerServiceProvider($inputs);
		}
		if($inputs['user_type'] == config('user.type.customer')){
			$user = $this->registerCustomer($inputs);
		}

		return $user;

	}

	/**
	 * Create new Service Provider
	 * @param  Array  $inputs
	 * @return App\Models\User
	 */
	public function registerServiceProvider(Array $inputs)
	{
		 $service_provider = ServiceProvider::create();
         $inputs['password'] = Hash::make($inputs['password']);

         $user = new User;
         $user->phone = array_get($inputs, 'phone');
         $user->password = array_get($inputs, 'password');
         $user->fill($inputs);

         $service_provider->user()->save($user);

         return $user;
	}

	/**
	 *  Create new Customer
	 * @param  Array  $inputs [description]
	 * @return App\Models\User
	 */
	public function registerCustomer(Array $inputs) {

		 $customer = Customer::create();
         
         $inputs['password'] = Hash::make($inputs['password']);

         $user = new User;
         $user->phone = array_get($inputs, 'phone');
         $user->password = array_get($inputs, 'password');
         $user->fill($inputs);

     $customer->user()->save($user);

         return $user;
	}


	/**
	 * Update User's Password
	 * @param  Array  $inputs [description]
	 * @return [type]         [description]
	 */
	public function updatePassword(Array $inputs)
	{

		$this->verifyOtp($inputs['phone'], $inputs['otp']);

		$inputs['password'] = Hash::make($inputs['password']);

		$userRepository = new UserRepository;

		$user = $userRepository->getByPhone($inputs['phone']);

		$user->password = $inputs['password'];

		$user->save();

	}





	public function verifyOtp($phone_number, $otp)
	{
		
		$is_otp_correct = $this->msg91->verifyOtp($phone_number, (int)$otp);

		if(!$is_otp_correct) {
			throw new InvalidOtpException("Invalid Otp");
		}
	
	}




}