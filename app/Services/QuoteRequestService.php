<?php

namespace App\Services;

use App\Models\JobCard;
use App\Models\QuoteRequest;

class QuoteRequestService
{
	public function __construct(ServiceProviderService $serviceProviderService)
	{
		$this->serviceProviderService = $serviceProviderService;
	}

	/**
	 * Create a Quote Request
	 * @param  JobCard $jobCard
	 * @return QuoteRequest
	 */
	public function create(JobCard $jobCard)
	{
		$quoteRequest = new QuoteRequest;

		$quoteRequest->status = config('request.status.active');

		$jobCard->quoteRequest()->save($quoteRequest);
		
		$nearby_service_providers = $this->serviceProviderService->getNearbyServiceProviders($jobCard->location_lat, $jobCard->location_long, env('DEFAULT_DISTANCE_FILTER_RANGE', 8));

		foreach ($nearby_service_providers as  $service_provider) {

			$service_provider->quoteRequests()->attach($quoteRequest->id);

		}
		return $quoteRequest;
	}

}