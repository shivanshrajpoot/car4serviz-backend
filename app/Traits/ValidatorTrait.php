<?php

namespace App\Traits;

use Validator;
use Illuminate\Validation\ValidationException;

trait ValidatorTrait {

	public function messages()
	{
		return [];
	}

	public function fire($inputs, $type = '', $data = []) 
	{
		$validator = Validator::make($inputs, $this->rules($inputs, $type, $data), $this->messages());

		if($validator->fails()) {
			throw new ValidationException($validator);
		}
	}
}