<?php

namespace App\Contracts;

interface ValidatorContract 
{
	function fire($inputs, $type = '');

	function rules($inputs, $type = '', $data = []);
}