<?php

namespace App\Http\Requests\BookingRequest;

use Gate;
use Illuminate\Foundation\Http\FormRequest;

class AcceptBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('booking_request.accept', $this->booking_request);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
