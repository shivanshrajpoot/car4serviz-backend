<?php

namespace App\Http\Requests\JobCard;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('job_card.create', $this->vehicle);
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location_lat' => 'numeric',
            'location_long' => 'numeric',
            'address' => 'string|max:255',
            'service_datetime' => 'date',
            'remark' => 'string',
            'services' => 'array|required|min:1',
            'services.*' => 'numeric|digits_between:1,5',
            'service_subcategories' => 'array',
            'service_subcategories.*' => 'numeric|exists:service_subcategories,id',
            'service_provider_id' => 'numeric|exists:service_providers,id'
        ];

    }
}
