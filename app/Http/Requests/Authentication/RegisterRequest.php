<?php

namespace App\Http\Requests\Authentication;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages() {
        return
        [ 'phone.unique' => 'Phone number is already registered' ]; 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:255',
            'email' => 'string|email|max:255|unique:users',
            'password' => 'required|min:6|max:255',
            'phone' => 'required|numeric|digits:10|unique:users',
            'user_type' => 'required|numeric|in:1,2'
        ];
    }
}
