<?php

namespace App\Http\Requests\Authentication;

use Illuminate\Foundation\Http\FormRequest;

class SendOtpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the custom messages for validation errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'phone.exists' => 'You are not registered yet'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // For Password Reset
        if(strpos($this->path(), 'reset-password') !== false) {
            return [
                'phone' => 'required|numeric|digits:10|exists:users'
            ];    
        }

        return [
            'phone' => 'required|numeric|digits:10|unique:users'
        ];
    }
}
