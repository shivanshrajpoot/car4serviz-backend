<?php

namespace App\Http\Requests\Vehicle;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'car_type_id' => 'required|numeric|exists:car_types,id',
            'registration_number' => 'required|string|max:255',
            'purchase_year' => 'required|numeric|digits:4',
            'fuel_type' => 'required|numeric|digits_between:1,4',
            'car_variant_id' => 'required|numeric|exists:car_variants,id'
        ];
    }
}
