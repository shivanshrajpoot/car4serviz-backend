<?php

namespace App\Http\Resources;

use App\Services\HelperSevice;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceProviderBooking extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $helperService = new HelperSevice;

        $job_card = $this->jobCard;
        
        $services =  $helperService->mapServicesAndCategories($job_card);
        $vehicle = $job_card->vehicle;

        return [
            'id' => $this->id,
            'status' => $this->staus_name,
            'total_amount' => $this->total_amount,
            'vehicle_name' => $vehicle->display_name,
            'services' => $services,
            'service_datetime' => '',
            'latitude' => $job_card->location_lat,
            'longitude' => $job_card->location_long,
            'service_datetime' => $job_card->service_datetime,
            'remark' => $job_card->remark,
            'service_location' => ucfirst (array_search( $job_card->service_location , config('jobcard.location') )),
            'service_started_at' => $this->service_started_at,
            'service_completed_at' => $this->service_completed_at

        ];
    }
}
