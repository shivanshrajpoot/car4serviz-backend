<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;

use App\Http\Requests\Vehicle\CreateRequest;

class VehicleController extends Controller
{	

	/**
	 * Get All Vehicles of a customer
	 * @return JSON Response
	 */
	public function index()
	{
		$customer = auth('api')->user()->userable;

		return response()->success($customer->vehicles);
	}
	
	public function store(CreateRequest $request)
	{
		$inputs = request()->all();

		$customer = auth('api')->user()->userable;

		$vehicle = new Vehicle;

		$vehicle->car_type_id = array_get($inputs, 'car_type_id');
		$vehicle->registration_number = array_get($inputs, 'registration_number');
		$vehicle->purchase_year = array_get($inputs, 'purchase_year');
		$vehicle->fuel_type = array_get($inputs, 'fuel_type');
		$vehicle->car_variant_id = array_get($inputs, 'car_variant_id');

		$vehicle = $customer->vehicles()->save($vehicle);

		return response()->success($vehicle); 

	}
}