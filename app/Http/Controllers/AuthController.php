<?php

namespace App\Http\Controllers;

use App\Models\User;

use App\Services\AuthService;
use App\Services\PhoneService;
use App\Services\NotificationService;

// Requests
use App\Http\Requests\Authentication\LoginRequest;
use App\Http\Requests\Authentication\SendOtpRequest;
use App\Http\Requests\Authentication\RegisterRequest;
use App\Http\Requests\Authentication\PasswordResetRequest;
use App\Http\Requests\Authentication\SaveFcmTokenRequest;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(AuthService $authService, PhoneService $phoneService, NotificationService $notificationService)
    {

        $this->authService = $authService;
        $this->phoneService = $phoneService;
        $this->notificationService = $notificationService;

    }


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        
        $credentials = request(['phone', 'password']);

        $token = auth('api')->attempt($credentials);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->error(['error' => 'Invalid credentials'], 400);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {

        if(auth()->user()->userable instanceof ServiceProvider) {
            return response()->success(auth()->user()->load(['userable.carTypes', 'userable.serviceCategories', 'userable.services']));
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $inputs = request()->all();

        $fcm_token = array_get($inputs, 'fcm_token');

        $user = auth('api')->user();

        if($fcm_token){
            $this->notificationService->removeFcmToken($user, $fcm_token);
        }

        auth()->logout();

        return response()->success(['message' => 'Successfully logged out']);
    }


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {   
        $user = auth('api')->user();
        $userable = $user->userable;
        $user_type = $user->type;

        $inputs = request()->all();
        $request_user_type = array_get($inputs, 'type');

        if($user->type != array_search($request_user_type, config('user.type'))) {
            return response()->error(['error' => 'Please Login as '.ucfirst($user_type) ], 400);   
        }

        return response()->success([
            'user' => $user,
            'user_type' => $user_type,
            $user_type => $userable,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }



    /**
     *
     * Send Otp for registration
     * 
     * @return JSON Response
     */
    public function sendOtp(SendOtpRequest $request)
    {
        $inputs = request()->all();

        $this->authService->sendOtp($inputs['phone']);

        return response()->success('Otp Sent');
    }


    /**
     *
     * 
     * @return JSON Response
     */
    public function resendOtp(SendOtpRequest $request)
    {
        $inputs = request()->all();
        
        $this->authService->resendOtp($inputs['phone']);

        return $this->success('Otp Sent');

    }

    /**
     * Register new User
     * @param  RegisterRequest $request
     * @return JSON Response
     */
    public function register(RegisterRequest $request)
    {
        $inputs = request()->all();
        $user = $this->authService->register($inputs);

        $credentials = request(['phone', 'password']);

        $token = auth('api')->attempt($credentials);
        
        return $this->respondWithToken($token); 
    }

    /**
     * Reset User's password
     * @param  PasswordResetRequest $request
     * @return JSON Response
     */
    public function resetPassword(PasswordResetRequest $request)
    {

        $inputs = request()->all();

        $this->authService->updatePassword($inputs);

        return response()->success('Password Changed Successfully');

    }

    public function deleteUser()
    {
        $inputs = request()->all();    

        $user = User::where('phone', $inputs['phone'])->first();

        $userable = $user->userable();

        $user->forceDelete();
        $userable->forceDelete();      

        return response()->success("Say thanks");  

    }

    /**
     * Save FCM token of the user
     * @param  SaveFcmRequest $request
     * @return JSON Response
     */
    public function saveFcmToken(SaveFcmTokenRequest $request)
    {
        $inputs = $request->all();

        $fcm_token = array_get($inputs, 'fcm_token');

        $user = auth('api')->user();

        $user = $this->notificationService->saveFcmToken($user, $fcm_token);

        return response()->success('Token Saved Successfully');

    }


}