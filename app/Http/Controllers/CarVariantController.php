<?php

namespace App\Http\Controllers;

use App\Models\CarModel;
use App\Models\CarBrand;
use App\Models\CarVariant;

use Illuminate\Http\Request;
use App\Services\CustomerService;

class CarVariantController extends Controller
{
	public function index(Request $request, $car_brand_id, $car_model_id)
	{	
		$car_model = CarModel::find($car_model_id);
		return response()->success($car_model->variants);
	}

	public function getAll()
	{

		return response()->success(CarVariant::appends(['model_name','brand_name']));
	}
}