<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function success($data)
    {
    	$status = 'success';

    	return response(compact('data', 'status'));
    }

    public function error($error)
    {
    	$status = 'error';

    	return response(compact('error', 'status'));
    }
}
