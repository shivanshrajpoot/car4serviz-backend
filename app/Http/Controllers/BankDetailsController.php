<?php

namespace App\Http\Controllers;

use App\Http\Requests\BankDetails\ViewRequest;

class BankDetailsController extends Controller {

	public function getBankDetails(ViewRequest $request) {

		$inputs = request()->all();
		$ifsc_code = array_get($inputs, 'ifsc_code');

		$client = new \GuzzleHttp\Client();
					
		try {
			$res = $client->request('GET', 'https://ifsc.razorpay.com/'.$ifsc_code);	
		}
		catch(\Exception $e) {
			return response()->error('Bank Details not found', 404);
		}
		
		return response()->success( ['bank_details' => json_decode($res->getBody()->getContents())] ); 


	}
}