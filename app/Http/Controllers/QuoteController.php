<?php

namespace App\Http\Controllers;


/**
 * Request Imports
 */
use Illuminate\Http\Request;
use App\Http\Requests\Quote\CreateRequest;
use App\Http\Requests\Quote\IndexRequest;

/**
 * Models
 */
use App\Models\JobCard;
use App\Models\QuoteRequest;

/**
 * Service Imports
 */
use App\Services\QuoteService;


class QuoteController extends Controller
{

    /**
     * Constructor 
     * @param QuoteService $quoteService
     */
    function __construct(QuoteService $quoteService)
    {
    	$this->quoteService = $quoteService;
    }


    /**
     * Give a Quote for Quote Request
     * @param  CreateRequest $request       
     * @param  QuoteRequest  $quote_request 
     * @return JSON Response
     */
    public function create(CreateRequest $request, QuoteRequest $quote_request)
    {
        if($quote_request->status !== config('request.status.active')) return response()->error("Quote Request is already fulfilled or cancelled", 409);

        $inputs = $request->all();

        $service_provider = auth()->user()->userable;

        if($quote_request->quotes()->where('service_provider_id',$service_provider->id)->count() > 0) return response()->error("You have already sent a quote for this request", 409);

        $quote = $this->quoteService->create($quote_request, $service_provider, $inputs);
        $quote->quoteRequest;

        $quote_request->serviceProviders()->detach($service_provider->id);

        return response()->success($quote);
    }

    /**
     * List All Quotes of a Job Card
     * @param  IndexRequest $request  
     * @param  JobCard      $job_card 
     * @return JSON Response
     */
    public function index( IndexRequest $request, JobCard $job_card )
    {
        if( $job_card->status !== config('jobcard.status.quote') ) 
            return response()->error("Quotes No Longer Available", 409);

        $quote_request = $job_card->quoteRequest;

        $quotes = $quote_request->quotes()->with('serviceProvider')->get();

        return response()->success($quotes);

    }


    public function accept(AcceptRequest $request, JobCard $job_card ,Quote $quote)
    {
        if($job_card->status !== config('job_card.status.quote')) return response()->error("You have already accepted another quote or cancelled this request", 409);

    }


}