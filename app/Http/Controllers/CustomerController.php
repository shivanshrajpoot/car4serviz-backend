<?php

namespace App\Http\Controllers;

use App\Services\CustomerService;
use App\Http\Requests\Customer\UpdateRequest;

class CustomerController extends Controller
{
	public function __construct(CustomerService $customerService)
	{
		$this->customerService = $customerService;
	}

	/**
	 * Update Model Customer
	 * @param  UpdateRequest $request
	 * @return JSON Response
	 */
	public function update(UpdateRequest $request)
	{
		$inputs = request()->all();

		$customer = auth()->user()->userable;

		$customer = $this->customerService->update($customer ,$inputs);

		$customer->user;

		return response()->success(['customer' => $customer]);
		
	}

	public function getJobCards()
	{
		$customer = auth()->user()->userable;
		
		$job_cards = $customer->jobCards()
						->where('status', '<>', config('jobcard.status.draft'))
						->where('status', '<>', config('jobcard.status.cancelled'))
						->get();

		return response()->success($job_cards);
	}
}