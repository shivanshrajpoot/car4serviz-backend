<?php

namespace App\Http\Controllers;

use App\Models\Service;

class ServicesController extends Controller
{

	public function __construct()
	{

	}

	/**
	 * Get All Services
	 * @return JSON Response
	 */
	public function index()
	{
		$services = Service::with('categories')->get();
		return response()->success($services);
	}
}