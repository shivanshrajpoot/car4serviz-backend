<?php

namespace App\Http\Controllers;

use App\Models\CarModel;
use App\Models\CarBrand;

use Illuminate\Http\Request;
use App\Services\CustomerService;

class CarModelController extends Controller
{
	public function index(Request $request, $car_brand_id)
	{	
		$car_brand = CarBrand::find($car_brand_id);
		return response()->success($car_brand->models);
	}
}