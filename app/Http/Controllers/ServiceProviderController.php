<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ServiceProviderService;
use App\Http\Requests\ServiceProvider\UpdateRequest;
use App\Http\Resources\ServiceProviderBooking;
use App\Http\Resources\ServiceProviderBookingCollection;

class ServiceProviderController extends Controller
{
    function __construct(ServiceProviderService $serviceProviderService)
    {
    	$this->serviceProviderService = $serviceProviderService;
    }

    /**
     * Get Service Providers list
     * @return JSON Response
     */
    public function index()
    {   
        $inputs = request()->all();

        $perPage = array_get($inputs, 'perPage', 15);
        $latitude = array_get($inputs, 'latitude');
        $longitude = array_get($inputs, 'longitude');
        $distance_range = array_get($inputs, 'range', env('DEFAULT_DISTANCE_FILTER_RANGE'));

        if($latitude && $longitude) {
            $service_providers = $this->serviceProviderService
                                      ->getPaginatedNearbyServiceProviders($latitude, $longitude, $distance_range, $perPage);
        }
        else {
            $service_providers = $this->serviceProviderService->getAllPaginated($perPage);
        }
        
        return response()->success($service_providers);
    }


    public function update(UpdateRequest $request, $step = null)
	{
		$service_provider = auth()->user()->userable;

		$inputs = request()->all();

		$service_provider = $this->serviceProviderService->edit($service_provider, $inputs, $step);

		return response()->success($service_provider);
	}

	public function updateWithSteps(UpdateRequest $request, $step)
	{
		return $this->update($request, $step);
	}

    public function getBookingRequests()
    {
        $service_provider = auth()->user()->userable;   
        return response()->success($service_provider->bookingRequests);
    }

    public function getQuoteRequests()
    {
        $service_provider = auth()->user()->userable;   
        return response()->success($service_provider->quoteRequests);
    }

    public function getBookings()
    {
        $inputs = request()->all();

        $perPage = array_get($inputs, 'perPage', 15);

        $service_provider = auth()->user()->userable;
        $bookings = $service_provider
                    ->bookings()
                    ->join('job_cards', 'job_cards.id', '=', 'bookings.job_card_id')
                    ->select('bookings.*','job_cards.service_datetime')
                    ->orderBy('job_cards.service_datetime')
                    ->paginate($perPage);

        return (new ServiceProviderBookingCollection($bookings))->additional(['success' => 'true', 'code' => 200]);
    }
}
