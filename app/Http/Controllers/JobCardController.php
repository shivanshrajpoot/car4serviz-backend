<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use App\Services\JobCardService;
use App\Http\Requests\JobCard\CreateRequest;

class JobCardController extends Controller
{
	/**
	 * 
	 * @param JobCardService $jobCardService [description]
	 */
	public function __construct(JobCardService $jobCardService)
	{
		$this->jobCardService = $jobCardService;
	}

	
	/**
	 * @param CreateRequest $createRequest
	 * @param Vehicle $vehicle
	 * @return  Boolean
	 */
	public function create(CreateRequest $createRequest, Vehicle $vehicle)
	{	

		$inputs = request()->all();

		$customer = auth()->user()->userable;

		$response = $this->jobCardService->create($customer, $vehicle ,$inputs);

		return response()->success($response);
	}


}