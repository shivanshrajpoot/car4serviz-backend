<?php

namespace App\Http\Controllers;

use App\Models\CarBrand;

use App\Services\CustomerService;

class CarBrandController extends Controller
{
	public function index()
	{
		return response()->success(CarBrand::get());
	}
}