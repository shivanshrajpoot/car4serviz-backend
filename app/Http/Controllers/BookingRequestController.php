<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingRequest;

use Illuminate\Http\Request;

use App\Events\BookingConfirmed;

use App\Services\BookingRequestService;

use App\Http\Requests\BookingRequest\AcceptBookingRequest;

class BookingRequestController extends Controller
{
    function __construct(BookingRequestService $bookingRequestService)
    {
    	$this->bookingRequestService = $bookingRequestService;
    }

    /**
     * Accept a Booking Request
     * @param  AcceptBookingRequest $request         
     * @param  BookingRequest       $booking_request 
     * @return JSON Response                                
     */
    public function acceptRequest(AcceptBookingRequest $request, BookingRequest $booking_request)
    {
        if($booking_request->status !== config('request.status.active')) return response()->error("Booking is already accepted or cancelled", 409);

        $booking = new Booking;

        $booking->total_amount = $booking_request->total_amount;
        $booking->job_card_id = $booking_request->job_card_id;
        $booking->booking_request_id = $booking_request->id;
        $booking->verification_code = mt_rand(1000, 9999);
        $booking->status = config('booking.status.active');

        $service_provider = auth()->user()->userable;

        $service_provider->bookings()->save($booking);

        event(new BookingConfirmed($booking));

        $booking->jobCard;
        $booking->serviceProvider;

        $booking_request->status = config('request.status.fulfilled');

        $booking_request->save();

        return response()->success($booking);

    }

    public function rejectRequest()
    {

    }
}