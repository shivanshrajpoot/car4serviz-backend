<?php

namespace App\Http\Controllers;

use App\Models\CarType;

class CarTypesController extends Controller
{

	public function __construct()
	{

	}

	/**
	 * Get All Car Types
	 * @return JSON Response
	 */
	public function index()
	{
		return response()->success(CarType::get());		
	}
}