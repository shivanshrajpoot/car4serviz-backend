<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Customer;

use App\Exceptions\AccessException;

class CustomerMiddleware
{
	public function handle($request, Closure $next, ...$attributes)
	{
		if(auth('api')->user()->userable instanceof Customer) {
			return $next($request);
		}

		throw new AccessException();
	}
}