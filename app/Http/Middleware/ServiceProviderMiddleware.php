<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\ServiceProvider;

use App\Exceptions\AccessException;

class ServiceProviderMiddleware
{
	public function handle($request, Closure $next, ...$attributes)
	{
		\Log::info($request->all());
		if(auth('api')->user()->userable instanceof ServiceProvider) {
			return $next($request);
		}

		throw new AccessException();
	}
}
