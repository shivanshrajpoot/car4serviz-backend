<?php

namespace App\Listeners;

use App\Events\BookingConfirmed;
use App\Services\NotificationService;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBookingConfirmationNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  BookingConfirmed  $event
     * @return void
     */
    public function handle(BookingConfirmed $event)
    {
        $booking = $event->booking;
        $job_card = $booking->jobCard;
        $vehicle = $job_card->vehicle;
        $customer = $vehicle->customer;
        $user = $customer->user;

        $service_provider = $booking->serviceProvider;

        $title = "Booking Confirmed";
        $notification = "You service booking for ".$vehicle->brand->name." ".$vehicle->model->name." has been confimed at ".$service_provider->garage_name;

        $data = $booking->toArray();

        $this->notificationService->notify($user, $title, $notification, $data);

        return false;

    }
}