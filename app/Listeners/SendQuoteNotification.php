<?php

namespace App\Listeners;

use App\Events\QuoteRecieved;
use App\Services\NotificationService;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendQuoteNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * Handle the event.
     *
     * @param  BookingConfirmed  $event
     * @return void
     */
    public function handle(QuoteRecieved $event)
    {
        $quote = $event->quote;
        $quote_request = $quote->quoteRequest;
        $job_card = $quote_request->jobCard;
        $vehicle = $job_card->vehicle;
        $customer = $vehicle->customer;
        $user = $customer->user;

        $service_provider = $quote->serviceProvider;
        
        $title = "New Quote Recieved";
        $notification = "You recieved a new quote for ".$vehicle->brand->name." ".$vehicle->model->name." from ".$service_provider->garage_name;

        $data = $quote->toArray();

        $this->notificationService->notify($user, $title, $notification, $data);

        return true;

    }
}