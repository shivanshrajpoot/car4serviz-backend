<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    
    /**
     * Authentication
     * 
     */
    Route::post('login', 'AuthController@login');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('send-otp', 'AuthController@sendOtp');
    Route::post('resend-otp', 'AuthController@sendOtp');
    Route::post('register', 'AuthController@register');

    /**
     * Password Reset or updation routes
     */
    Route::post('send-otp/reset-password', 'AuthController@sendOtp');
    Route::post('reset-password', 'AuthController@resetPassword');

    Route::post('dlete-user', 'AuthController@deleteUser');

    /**
     *  Bank Details
     */
    Route::get('bank-details', 'BankDetailsController@getBankDetails');


    /**
     * Routes which requires user to be logged in
     */
    Route::group([
    	'middleware' => 'auth:api'
    ], function($router) {
    	
        /**
         * Autentication Routes
         * 
         */
        Route::get('me', 'AuthController@me');
    	Route::post('logout', 'AuthController@logout');
        Route::patch('fcm-token', 'AuthController@saveFcmToken');


        Route::get('service-providers', 'ServiceProviderController@index');
    	

        /**
         * Routes only service providers can access
         */
        
        Route::group(['middleware' => 'service-providers'], function($router) {

            Route::put('service-providers', 'ServiceProviderController@update');
            Route::post('service-providers/step/{step}', 'ServiceProviderController@updateWithSteps');
            
            Route::get('service-providers/booking-requests','ServiceProviderController@getBookingRequests');
            Route::get('service-providers/quote-requests','ServiceProviderController@getQuoteRequests');

            Route::get('service-providers/bookings','ServiceProviderController@getBookings');            

            Route::patch('service-providers/booking-requests/{booking_request}/accept','BookingRequestController@acceptRequest');

            Route::post('service-providers/quote-requests/{quote_request}/quote', 'QuoteController@create');
        
        });


        
        /**
         * Routes only cutomers can access
         */
        Route::group(['middleware' => 'customers'], function($router) {

        Route::post('customers', 'CustomerController@update');

        Route::get('customers/vehicles', 'VehicleController@index');

        Route::post('customers/vehicles', 'VehicleController@store');

        Route::post('customers/vehicles/{vehicle}/job-cards','JobCardController@create')->name('customers.job_card.create');

        Route::get('customers/job-cards', 'CustomerController@getJobCards');

        Route::get('customers/job-cards/{job_card}/quotes', 'QuoteController@index');
        
        });


        
		


        /**
         * Services 
         */
        Route::get('services', 'ServicesController@index');
		
        /**
         * Car Types
         */
        Route::get('car-types', 'CarTypesController@index');


        Route::get('car-brands', 'CarBrandController@index');

        Route::get('car-brands/{car_brand_id}/models', 'CarModelController@index');

        Route::get('car-brands/{car_brand_id}/models/{car_model_id}/variants', 'CarVariantController@index');





    });

