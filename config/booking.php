<?php

return [
	"status" => [
		"active" => 1,
		"ongoing" => 2,
		"completed" => 3,
		"paid" => 4,
		"cancelled" => 5
	]
];