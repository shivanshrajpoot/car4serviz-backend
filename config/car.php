<?php

return [
	"fuel_type" => [
		"Petrol" => 1,
		"Diesel" => 2,
		"CNG" => 3,
		"Battery" => 4
	]
];	