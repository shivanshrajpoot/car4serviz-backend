<?php

return [
	"type" => [
		"general" => 0,
		"roadside" => 1,
		"other" => 2
	],
	"status" => [
		"draft" => 0,
		"quote" => 1,
		"booking_request" => 2,
		"booking" => 3,
		"completed" => 4,
		"cancelled" => 5,
	],
	"inital_status" => [
		0 => 2,
		1 => 2,
		2 => 1
	],
	"location" => [
		"vendor" => 1,
		"customer" => 2
	]

];