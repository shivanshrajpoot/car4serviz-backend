<?php

return [
	"status" => [
		"active" => 1,
		"fulfilled" => 2,
		"cancelled" => 3
	]
];